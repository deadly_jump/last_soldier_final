﻿using UnityEngine;
using System.Collections;

public class playercontroller : MonoBehaviour {
    public float runSpeed;
    public float walk;

    Animator animacion;
    Rigidbody realidad;
    bool facingRight;

    bool grounded = false;

    Collider[] groungcollisioner;
    float groundCheckRadius = 0.2f;
    public LayerMask groundLayer;
    public Transform groundCheck;
    public float jumpHeight;


	// Use this for initialization
	void Start () {
        realidad = GetComponent<Rigidbody>();
        animacion = GetComponent<Animator>();
        facingRight = true;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void FixedUpdate()

    {       
        if (grounded && Input.GetAxis("Jump")> 0) {
            grounded = false; 
            animacion.SetBool("grounded",grounded);
            realidad.AddForce(new Vector3 (0,jumpHeight,0));
        }

        groungcollisioner = Physics.OverlapSphere(groundCheck.position,groundCheckRadius,groundLayer);
        if (groungcollisioner.Length > 0) grounded = true;
        else grounded = false;
        animacion.SetBool("grounded",grounded);


        float move = Input.GetAxis("Horizontal");
        animacion.SetFloat("speed",Mathf.Abs(move));
        
        float sneaking = Input.GetAxisRaw("Fire3");
        animacion.SetFloat("sneaking", sneaking);

        if (sneaking > 0)
        {
            realidad.velocity = new Vector3(move * walk, realidad.velocity.y, 0);
        }
        else
        {
            realidad.velocity = new Vector3(move * runSpeed, realidad.velocity.y, 0);
        }
        
        if (move > 0 && !facingRight) Flip();
        else if (move < 0 && facingRight) Flip();

       
    }
    void Flip()
    {
        facingRight = !facingRight;
        Vector3 escalaPj = transform.localScale;
        escalaPj.z *= -1;
        transform.localScale = escalaPj;
    }
}
