﻿using UnityEngine;
using System.Collections;



    public class PlayerShooting : MonoBehaviour
    {
        public int damagePerShot = 20;                  // The damage inflicted by each bullet.
        public float timeBetweenBullets = 0.15f;        // The time between each shot.
        public float range = 100f;                      // The distance the gun can fire.

        EnemyHealth enemy;
        float timer;                                    // A timer to determine when to fire.
        Ray shootRay;                                   // A ray from the gun end forwards.
        RaycastHit shootHit;                            // A raycast hit to get information about what was hit.
        int shootableMask;                              // A layer mask so the raycast only hits things on the shootable layer.
        ParticleSystem gunParticles;                    // Reference to the particle system.
        LineRenderer gunLine;                           // Reference to the line renderer.
        AudioSource gunAudio;                           // Reference to the audio source.
        Light gunLight;                                 // Reference to the light component.
        public Light faceLight;								// Duh
        float effectsDisplayTime = 0.2f;                // The proportion of the timeBetweenBullets that the effects will display for.
                                                        // explosive explosiones;
    //public GameObject barriles;
    public explosive explosion;
   
    //Dejar de moverse
    // PlayerController playerMovement;
    public Animator anim;
    //public PlayerController Player;

    void Awake()
        {
       // anim = GetComponent<Animator>();


        // Create a layer mask for the Shootable layer.
        shootableMask = LayerMask.GetMask("Shootable");

            // Set up the references.
            gunParticles = GetComponent<ParticleSystem>();
            gunLine = GetComponent<LineRenderer>();
            gunAudio = GetComponent<AudioSource>();
            gunLight = GetComponent<Light>();
            faceLight = GetComponentInChildren<Light> ();


        //explosion=Instantiate<explosive>(explosion);

        
        /// = barreGetComponent<explosive>();
        //explosion w

        /*Dejar de moverse*/
        // playerMovement = GetComponent<PlayerController>();

    }


        void Update()
        {
            // Add the time since Update was last called to the timer.
            timer += Time.deltaTime;


            // If the Fire1 button is being press and it's time to fire...
            if (Input.GetButton("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0)
            {
                // ... shoot the gun.
                Shoot();
            }

            // If the timer has exceeded the proportion of timeBetweenBullets that the effects should be displayed for...
            if (timer >= timeBetweenBullets * effectsDisplayTime)
            {
                // ... disable the effects.
                DisableEffects();
            }
        }


        public void DisableEffects()
        {
            // Disable the line renderer and the light.
           gunLine.enabled = false;
           faceLight.enabled = false;
           gunLight.enabled = false;
           anim.SetBool("Disparar", false);
        //Player
        // Dejar de Mover Player
        //  playerMovement.enabled = false;

    }


        void Shoot()
        {

    
        // Reset the timer.
        timer = 0f;

            // Play the gun shot audioclip.
            gunAudio.Play();

            // Enable the lights.
            gunLight.enabled = true;
            faceLight.enabled = true;

            // Stop the particles from playing if they were, then start the particles.
            gunParticles.Stop();
            gunParticles.Play();

            // Enable the line renderer and set it's first position to be the end of the gun.
            gunLine.enabled = true;
            gunLine.SetPosition(0, transform.position);

            anim.SetBool("Disparar", true);


            // Set the shootRay so that it starts at the end of the gun and points forward from the barrel.
        shootRay.origin = transform.position;
            shootRay.direction = transform.forward;

            // Perform the raycast against gameobjects on the shootable layer and if it hits something...
            if (Physics.Raycast( shootRay, out shootHit, range,  shootableMask))
            {
                if (shootHit.collider.tag == "Enemy") {
                                 // Try and find an EnemyHealth script on the gameobject hit.
                                EnemyHealth enemyHealth = shootHit.collider.GetComponent<EnemyHealth>();
                                 Debug.Log("ENEMIGO HERIDO");
                                // If the EnemyHealth component exist...
                                if (enemyHealth != null)
                                {
                                        // ... the enemy should take damage.
                                     enemyHealth.TakeDamage(damagePerShot, shootHit.point);
                                }



                        // Set the second position of the line renderer to the point the raycast hit.
                        

                    }else if (shootHit.collider.tag== "Barrel" || shootHit.collider.tag=="FireBarrel")
                        {
                              //explosion=shootHit.collider.gameObject.GetComponent<explosive>();
                             
                              //
                //explosion.hitpoints = 0f;
                shootHit.collider.gameObject.tag = "explode";
                Debug.Log(shootHit.collider.tag);
                //explosion.hitpoints= 0f;
                //Debug.Log(explosiones.hitpoints);
                //Destroy(shootHit.collider.gameObject);
            }
                    gunLine.SetPosition(1, shootHit.point);
            }
            // If the raycast didn't hit anything on the shootable layer...
            else
            {
                // ... set the second position of the line renderer to the fullest extent of the gun's range.
                gunLine.SetPosition(1, shootRay.origin + shootRay.direction * range);
            }
        }
    }

