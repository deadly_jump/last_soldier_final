﻿using UnityEngine;
using System.Collections;
//using UnitySampleAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour {
 
    public float walkspeed = 10f;
    public float runSpeed = 20f;
    public Animator movimientosCharacterPrincipal;
    
    private Rigidbody rb;
    private int count;

    public float timeBetweenBullets = 0.15f;        // The time between each shot.
    public float timer;                                    // A timer to determine when to fire.
    public float effectsDisplayTime = 0.2f;                // The proportion of the timeBetweenBullets that the effects will display for.
	public GameObject characterPrincipal;

    /******************/
    public float speed = 6f;
    Vector3 movement;
    //////////////////////////////////////////Animator anim;
    Rigidbody playerRigidbody;
    int floorMask;
    float camRayLength = 100F;
     float h;
     float v;

    /********************/
    public bool estoySaltando = false;
    public float fuerzaSalto = 500;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
       //////////////////////////// anim = GetComponent<Animator>();
        playerRigidbody = GetComponent<Rigidbody>();
        floorMask = LayerMask.GetMask("Floor");
    }

   // void Start()
  //  {
       // rb = GetComponent<Rigidbody>();
      /*  count = 0;
        SetCountText();
        winText.text = "";*/
 //   }
 /*
    // Update is called once per frame
   void Update() {



        float moveHorizontal = walkspeed * Input.GetAxis("Horizontal") * Time.deltaTime;
        float moveVertical = walkspeed * Input.GetAxis("Vertical") * Time.deltaTime;

        Vector3 movement = new Vector3(-moveVertical, 0f, moveHorizontal);
        transform.Translate(movement);

        Turning();
        Animating(moveHorizontal, moveVertical);
        //playerRigidbody.MovePosition(transform.position + movement);

        // movimientosCharacterPrincipal.SetFloat("Movimiento", movement.magnitude);

     


        //Disparos
        // Add the time since Update was last called to the timer.
        timer += Time.deltaTime;
        // If the Fire1 button is being press and it's time to fire...
        if (Input.GetButton("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0)
        {
            // ... shoot the gun.
            Shoot();
        }
        // If the timer has exceeded the proportion of timeBetweenBullets that the effects should be displayed for...
        if (timer >= timeBetweenBullets * effectsDisplayTime)
        {
            // ... disable the effects.
            DisableEffects();
        }



    }*/



    void FixedUpdate()
    {

        h =  Input.GetAxisRaw("Horizontal");
         v = Input.GetAxisRaw("Vertical") ;
       // v = 0.0f;
      //  h = Input.GetAxisRaw("Horizontal");
       //  v = Input.GetAxisRaw("Vertical");
      
        Move(h, v);
        Turning();
        // Animating(h, v);
       // YaSalto();
        /*       //Disparos

              timer += Time.deltaTime;
               // If the Fire1 button is being press and it's time to fire...
               if (Input.GetButton("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0)
               {
                   // ... shoot the gun.
                   // Shoot();
                   Debug.Log("si dispa");

               }
               // If the timer has exceeded the proportion of timeBetweenBullets that the effects should be displayed for...
               if (timer >= timeBetweenBullets * effectsDisplayTime)
               {
                   // ... disable the effects.
                   DisableEffects();
               }*/

    }

   /*void OnCollisionStay(Collision collision)
    {
        Debug.Log(collision.gameObject.name);
    }*/



    void Move(float h, float v) {
        YaSalto();

        /* movement.Set(-v, 0f, h);
         movement = movement.normalized * speed * Time.deltaTime;
         transform.Translate(movement);*/
        // playerRigidbody.MovePosition(transform.position + movement);
        //   Debug.Log("mov" + movement);

        // Input.GetAxis("Horizontal"

       
     


      //  if (input.sqrMagnitude >= 0.3f){


			/*var newRotation = Quaternion.LookRotation(transform.position - target.position, Vector3.forward);
			newRotation.x = 0.0f;
			newRotation.z = 0.0f;
			transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime * 8);*/
		/*characterPrincipal.transform.rotation =  Quaternion.LookRotation( input,characterPrincipal.transform.up);
		}
		transform.Translate(input*speed*Time.deltaTime);
		movimientosCharacterPrincipal.SetFloat ("Movimiento",  input.sqrMagnitude*speed  );*/
	//Input.GetAxis("Horizontal")

	Vector3 input = new Vector3(0f, 0f, Input.GetAxis("Horizontal"));
	transform.Translate(input*speed*Time.deltaTime);
	movimientosCharacterPrincipal.SetFloat ("Movimiento",  input.sqrMagnitude*speed  );

	/*if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W) )
	{
		Vector3 position = this.transform.position;
		position.x++;
		//this.transform.position = position;
		transform.Translate(position*speed*Time.deltaTime);
	}*/
        //Detectar Si esta corriendo
        if (Input.GetButton("Run"))
        {
            speed = runSpeed;

        }
        
        if (Input.GetButtonUp("Run"))
        {
            speed = walkspeed;
        }
        //Detectar si esta saltando
	if (Input.GetKey(KeyCode.Space))
        {
            Saltar();
            // movimientosCharacterPrincipal.SetFloat("Movimiento", -0.2f);
        
        }

    }


     void Saltar()
    {
        if (estoySaltando == true)
        {
            Debug.Log("saLTO YA");
            return;
        }

        Debug.Log("aCTIVO EL SALTO");
        movimientosCharacterPrincipal.SetBool("Saltar", true);
         CancelAnimations();
        rb.AddForce(transform.up * fuerzaSalto);
          estoySaltando = true;
        //Invoke ("SetEstaCayendo",0.2f);
    }

    public void YaSalto()
    {

        estoySaltando = false;
        movimientosCharacterPrincipal.SetBool("Saltar", false);
    }
    /*void Shoot()
    {
        // Reset the timer.
        timer = 0f;

       // movimientosCharacterPrincipal.SetBool("Idle", false);
      //  movimientosCharacterPrincipal.SetFloat("Movimiento", -0.7f);
        movimientosCharacterPrincipal.SetBool("Disparar", true);
      //  movimientosCharacterPrincipal.enabled = false;

        // enabled = false;
        // Animating(h, v);

    }*/

  /*  public void DisableEffects()
    {


        //movimientosCharacterPrincipal.SetFloat("Movimiento", -0.2f);
        movimientosCharacterPrincipal.SetBool("Disparar", false);
       // movimientosCharacterPrincipal.SetBool("Idle", true);
     //  movimientosCharacterPrincipal.enabled = true;
     //   enabled = true;

        // Disable the line renderer and the light.
        /*gunLine.enabled = false;
        faceLight.enabled = false;
        gunLight.enabled = false;
    
    }*/

    void Turning()
    {

       // Debug.Log("Turning"+ Input.mousePosition);

        // Create a ray from the mouse cursor on screen in the direction of the camera.
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        // Create a RaycastHit variable to store information about what was hit by the ray.
        RaycastHit floorHit;

       

        // Perform the raycast and if it hits something on the floor layer...
        if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
        {            
            // Create a vector from the player to the point on the floor the raycast from the mouse hit.
            Vector3 playerToMouse = floorHit.point - transform.position;

            // Ensure the vector is entirely along the floor plane.
            playerToMouse.y = 0f;

            // Create a quaternion (rotation) based on looking down the vector from the player to the mouse.
            Quaternion newRotatation = Quaternion.LookRotation(playerToMouse);

            // Set the player's rotation to this new rotation.
            playerRigidbody.MoveRotation(newRotatation);
        }
        //Debug.Log("ghiy");
    }

    /*void Animating(float h, float v)
    {

       // Debug.Log("Wlk");
        // Create a boolean that is true if either of the input axes is non-zero.
        bool walking = !((v == 0) && (h == 0));//f || v != 0f;

        // Tell the animator whether or not the player is walking.
        anim.SetBool("IsWalking", !walking);
    }*/


	void CancelAnimations(){
		movimientosCharacterPrincipal.SetBool("Disparar", false);

	}

}
