﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour {


        public int startingHealth = 100;            // The amount of health the enemy starts the game with.
        public int currentHealth;                   // The current health the enemy has.
        public float sinkSpeed = 2.5f;              // The speed at which the enemy sinks through the floor when dead.
        public int scoreValue = 10;                 // The amount added to the player's score when the enemy dies.
        public AudioClip deathClip;                 // The sound to play when the enemy dies.

    /***********************/
    //Animator anim;                              // Reference to the animator.
        Animation anim;
        AudioSource enemyAudio;                     // Reference to the audio source.
        ParticleSystem hitParticles;                // Reference to the particle system that plays when the enemy is damaged.
        CapsuleCollider capsuleCollider;            // Reference to the capsule collider.
        bool isDead;                                // Whether the enemy is dead.
        bool isSinking;                             // Whether the enemy has started sinking through the floor.

        MoveEnemy moveE;
        EnemyAttack_AS attack;


    //public GameObject objetoParaAnimar;
    
    public AnimationClip morir;
    private Animation animacion;



    void Awake()
        {

       
        // Setting up the references.
                    anim = GetComponent<Animation>();
       enemyAudio = GetComponent<AudioSource>();
        hitParticles = GetComponentInChildren<ParticleSystem>();
        capsuleCollider = GetComponent<CapsuleCollider>();
        moveE = GetComponent<MoveEnemy>();
        attack = GetComponent<EnemyAttack_AS>();
        // Setting the current health when the enemy first spawns.
        currentHealth = startingHealth;



        }
    void Start()
    {
       // anim = objetoParaAnimar.AddComponent<Animation>();
       // anim.AddClip
        
        anim.AddClip(morir, "death");
    }

    void Update()
        {
            // If the enemy should be sinking...
            if (isSinking)
            {
           // Debug.Log("Sijjins");
                // ... move the enemy down by the sinkSpeed per second.
                transform.Translate(-Vector3.up * sinkSpeed * Time.deltaTime);
            }
        }


        public void TakeDamage(int amount, Vector3 hitPoint)
        {
        // Debug.Log("TakeDamage");
        // If the enemy is dead...
        // if (isDead)
        // ... no need to take damage so exit the function.
        //        return;
        Debug.Log("Hurting enemy");
            // Play the hurt sound effect.
           // enemyAudio.Play();

            // Reduce the current health by the amount of damage sustained.
            currentHealth -= amount;

            // Set the position of the particle system to where the hit was sustained.
            hitParticles.transform.position = hitPoint;

            // And play the particles.
            hitParticles.Play();

            // If the current health is less than or equal to zero...
            if (currentHealth <= 0)
            {
                // ... the enemy is dead.
                Death();
            }
       
    }


        void Death()
        {

 
        // The enemy is dead.
        isDead = true;

            // Turn the collider into a trigger so shots can pass through it.
         capsuleCollider.isTrigger = true;



        anim.Play("death");
       // capsuleCollider.enabled = false;
       // transform.Translate(-Vector3.up * 1000 * Time.deltaTime);
        // Tell the animator that the enemy is dead.
        //anim.SetTrigger("Dead");
        //anim.Play("death");
        //anim["death"].layer = 1;

        //anim["death"].wrapMode = WrapMode.Loop;
        //anim.Play("death");

        // Change the audio clip of the audio source to the death clip and play it (this will stop the hurt clip playing).
        enemyAudio.clip = deathClip;
            enemyAudio.Play();
        // enemyAudio.volume = 1;
        // Debug.Log(deathClip + "ssss");
        SheduleWaitLevel();
        ///

    }

    public void SheduleWaitLevel()
    {
        StartCoroutine(Wait(5));
    }

    IEnumerator Wait(int seconds)
    {

        StartSinking();

        yield return new WaitForSeconds(seconds);

        Destroy(gameObject, 0.0f);


    }



    public void StartSinking()
        {

        moveE.dead = true;
        attack.dead = true;
        // Find and disable the Nav Mesh Agent.
        GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = false;

            // Find the rigidbody component and make it kinematic (since we use Translate to sink the enemy).
            GetComponent<Rigidbody>().isKinematic = true;

            // The enemy should no sink.
           // isSinking = true;

            // Increase the score by the enemy's score value.
            ScoreManager.score += scoreValue;

            // After 2 seconds destory the enemy.
            
       // capsuleCollider.enabled = true;
    }
    }

