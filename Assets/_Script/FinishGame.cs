﻿using UnityEngine;
using System.Collections;

public class FinishGame : MonoBehaviour {

    GameObject Player;                          // Reference to the player GameObject.

    public AudioClip deathClip;                                 // The audio clip to play when the player dies.
   public Animator anim;                                              // Reference to the Animator component.
    AudioSource playerAudio;                                    // Reference to the AudioSource component.
                                                                // Setting up the references.


    void Awake()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
       // anim = GetComponent<Animator>();
        playerAudio = GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider other)
    {
        // If the exiting collider is the player...
        if (other.gameObject == Player)
        {

            Debug.Log("Finish");
            anim.SetFloat("Movimiento", -0.23f);
            anim.SetBool("Idle", true);
            anim.SetBool("Idle", false);
            //anim.Stop();
            //anim.Play("Win",1);
            anim.SetTrigger("Win");
        
            // Set the audiosource to play the death clip and play it (this will stop the hurt sound from playing).
            playerAudio.clip = deathClip;
            playerAudio.Play();
            // ... the player is no longer in range.
            //  playerInRange = false;
        }
    }

}
