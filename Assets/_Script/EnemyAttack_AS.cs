﻿using UnityEngine;
using System.Collections;


public class EnemyAttack_AS : MonoBehaviour
{
    public float timeBetweenAttacks = 0.5f;     // The time in seconds between each attack.
    public int attackDamage = 10;               // The amount of health taken away per attack.
    public bool dead = false;

    //  Animator anim;                              // Reference to the animator component.
    /*Armando*/
    //  Animation animation;
    //public string ataque;
    Animation anim;
    GameObject player;                          // Reference to the player GameObject.
    PlayerLife playerHealth;                  // Reference to the player's health.
//    EnemyHealth enemyHealth;                                        // EnemyHealth enemyHealth;                    // Reference to this enemy's health.
    bool playerInRange;                         // Whether player is within the trigger collider and can be attacked.
    float timer;                                // Timer for counting up to the next attack.


    void Awake()
    {
        // Setting up the references.
        player = GameObject.FindGameObjectWithTag("Player");
        playerHealth = player.GetComponent<PlayerLife>();
       
        //  enemyHealth = GetComponent<EnemyHealth>();
        //   anim = GetComponent<Animator>();
    }
    void Start()
    {
        anim = GetComponent<Animation>();
        // anim.AddClip

       // anim.AddClip(ataque, "attack1");
    }

    void OnTriggerEnter(Collider other)
    {
        // If the entering collider is the player...
        if (other.gameObject == player)
        {
            // ... the player is in range.
            playerInRange = true;
        }
    }


    void OnTriggerExit(Collider other)
    {
        // If the exiting collider is the player...
        if (other.gameObject == player)
        {
            // ... the player is no longer in range.
            playerInRange = false;
        }
    }


    void Update()
    {
        // Add the time since Update was last called to the timer.
        timer += Time.deltaTime;

        // If the timer exceeds the time between attacks, the player is in range and this enemy is alive...
        if (timer >= timeBetweenAttacks && playerInRange && !dead)/* && enemyHealth.currentHealth > 0)*/
        {
            // ... attack.
            Attack();
        }

        // If the player has zero or less health...
        if (playerHealth.currentHealth <= 0)
        {
            // ... tell the animator the player is dead.

          ///7  animation.tag
            //anim.SetTrigger("PlayerDead");
        }
    }


    void Attack()
    {
        // Reset the timer.
        anim.Play("attack1");
        timer = 0f;

        // If the player has health to lose...
        if (playerHealth.currentHealth > 0)
        {
            // ... damage the player.
            playerHealth.TakeDamage(attackDamage);
        }
    }
}