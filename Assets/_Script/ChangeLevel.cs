﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ChangeLevel : MonoBehaviour {
    GameObject Player;                          // Reference to the player GameObject.


    // Use this for initialization

    void Awake()
    {
              Player = GameObject.FindGameObjectWithTag("Player");
    }
 
    void OnTriggerEnter(Collider other)
    {
        // If the exiting collider is the player...
        if (other.gameObject == Player)
        {

            
            SceneManager.LoadScene("Cargando", LoadSceneMode.Single);
            // ... the player is no longer in range.
            //  playerInRange = false;
        }
    }
}
