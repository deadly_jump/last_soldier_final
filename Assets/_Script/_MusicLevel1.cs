﻿using UnityEngine;
using System.Collections;

public class _MusicLevel1 : MonoBehaviour {

    AudioSource playerAudio;
     public AudioClip levelSound;
    // Use this for initialization

    private void Awake()
    {
        playerAudio = GetComponent<AudioSource>();
    }
    void Start()
    {
        playerAudio.clip = levelSound;
        playerAudio.Play();
      //  playerAudio.loop = true;
    }


}
