﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class FinJuego : MonoBehaviour {
    GameObject Player;                          // Reference to the player GameObject.


    // Use this for initialization

    void Awake()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
    }



    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Puerta");
        Debug.Log(other.name);
        // If the exiting collider is the player...
        if (other.gameObject == Player)
        {


            SceneManager.LoadScene("Fin", LoadSceneMode.Single);
            // ... the player is no longer in range.
            //  playerInRange = false;
        }
    }
}
