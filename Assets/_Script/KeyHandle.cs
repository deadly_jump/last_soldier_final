﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyHandle : MonoBehaviour {

	public GameObject[] barrels;
    public int scoreValue = 20;
    //  public AudioClip KeySound;
    // Use this for initialization
    void Start () {
		barrels = GameObject.FindGameObjectsWithTag("FireBarrel");
        GameObject rand = barrels[Random.Range(0, barrels.Length-1)];

		Vector3 randPos = rand.transform.position;

		transform.position= new Vector3(randPos.x,randPos.y,randPos.z);
		Debug.Log(randPos);
	}

	void OnTriggerEnter(Collider other)
    {
        //=GameObject.FindWithTag("SF_Door");
        if (other.name == "Player")
        {
        	Debug.Log("key found");
            other.gameObject.layer = 11;
            Destroy(gameObject);
            //playerAudio.clip = KeySound;
            ScoreManager.score += scoreValue;

        }
  
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
