﻿using UnityEngine;
using System.Collections;

public class explosive : MonoBehaviour {
	public float hitpoints = 100f;
	public Transform spawnobject;
	public GameObject explosion;
	public float radius = 3.0f;
	public float power = 100.0f;
	// Use this for initialization
	void Start () {
	
	}

    void SetHitpoints(float variable)
    {
        this.hitpoints = variable;
    }
	
	// Update is called once per frame
	void Update () 
	{
		if (hitpoints <= 0)
		{
            Debug.Log("Barril");
			Instantiate(spawnobject, transform.position, transform.rotation);
			Instantiate(explosion, transform.position, Quaternion.identity);
			Vector3 explosionPos = transform.position;
			Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
			foreach (Collider hit in colliders) 
			{
				if (hit.GetComponent<Rigidbody>() != null)
				{
					Rigidbody rb = hit.GetComponent<Rigidbody>();
					rb.AddExplosionForce(power, explosionPos, radius, 3.0f);

				}
			}
			Destroy (gameObject);
		}
       
        if (gameObject.tag == "explode")
        {
            hitpoints = 0f;
        }
	}
	void Damage (float damage) 
	{
		

		hitpoints = hitpoints - damage;
	}

   /* void OnCollisionEnter(Collider other)
    {
        //=GameObject.FindWithTag("SF_Door");
        Debug.Log(other.name);
    }*/
}
