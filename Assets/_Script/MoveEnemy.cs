﻿using UnityEngine;
using System.Collections;

public class MoveEnemy : MonoBehaviour {

    Transform player;
    public bool dead= false; 
    UnityEngine.AI.NavMeshAgent nav;
    // Use this for initialization

 


	void Awake () {
        player = GameObject.FindGameObjectWithTag("Player").transform;

        nav = GetComponent<UnityEngine.AI.NavMeshAgent>();

      

	}


    // Update is called once per frame
    void Update () {
        if (!dead)
        {

            nav.SetDestination(player.position);
        }
        
	}
}
