﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerLife : MonoBehaviour {


    public int startingHealth = 100;                            // The amount of health the player starts the game with.
    public int currentHealth;                                   // The current health the player has.
    public Slider healthSlider;                                 // Reference to the UI's health bar.
    public Image damageImage;                                   // Reference to an image to flash on the screen on being hurt.
    public AudioClip deathClip;                                 // The audio clip to play when the player dies.
                                
    public float flashSpeed = 5f;                               // The speed the damageImage will fade at.
    public Color flashColour = new Color(1f, 0f, 0f, 0.3f);     // The colour the damageImage is set to, to flash.

    public AudioClip PowerUpSound;
    public AudioClip KeySound;
    public AudioClip AudioGolpe;

    Animator anim;                                              // Reference to the Animator component.
    AudioSource playerAudio;                                    // Reference to the AudioSource component.
    PlayerController playerMovement;                              // Reference to the player's movement.
    PlayerShooting playerShooting;                              // Reference to the PlayerShooting script.
    bool isDead;                                                // Whether the player is dead.
    bool damaged;


    GameObject PowerUp;                          // Reference to the player GameObject.
 //   int PowerUpMask;                              // A layer mask so the raycast only hits things on the shootable layer.

    void Awake()
    {
        // Setting up the references.
        anim = GetComponent<Animator>();
        playerAudio = GetComponent<AudioSource>();
        playerMovement = GetComponent<PlayerController>();
       playerShooting = GetComponentInChildren<PlayerShooting>();

        // Set the initial health of the player.
        currentHealth = startingHealth;
      //  PowerUpMask = LayerMask.GetMask("PowerUp");

      //  PowerUp = GameObject.FindGameObjectWithTag("PowerUp");
            
    }
   

    // Update is called once per frame
    void Update()
    {
        // If the player has just been damaged...
        if (damaged)
        {
            // ... set the colour of the damageImage to the flash colour.
            damageImage.color = flashColour;
        }
        // Otherwise...
        else
        {
            // ... transition the colour back to clear.
            damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }

        // Reset the damaged flag.
        damaged = false;
    }

    public void TakeDamage(int amount)
    {

       
        // Set the damaged flag so the screen will flash.
        damaged = true;

        // Reduce the current health by the damage amount.
        currentHealth -= amount;

        // Set the health bar's value to the current health.
        healthSlider.value = currentHealth;

        // Play the hurt sound effect.

        playerAudio.clip = AudioGolpe;
            playerAudio.Play();

        // If the player has lost all it's health and the death flag hasn't been set yet...
        if (currentHealth <= 0 && !isDead)
        {
            // ... it should die.
            Death();
        }
    }

    void Death()
    {
        // Set the death flag so this function won't be called again.
        isDead = true;

        // Turn off any remaining shooting effects.
        playerShooting.DisableEffects ();
        //Debug.Log("murio");
        // Tell the animator that the player is dead.
        anim.SetFloat("Movimiento", 0.0f);
        anim.SetBool("Idle", false);
        anim.SetTrigger("Die");
       
      

        // Set the audiosource to play the death clip and play it (this will stop the hurt sound from playing).
        playerAudio.clip = deathClip;
        playerAudio.Play();

        // Turn off the movement and shooting scripts.
        playerMovement.enabled = false;
        playerShooting.enabled = false;

		//SheduleRestarLevel ();


    }

    /*public void SheduleRestarLevel()
	{
		StartCoroutine (RestartLevel ());
	}

    IEnumerator RestartLevel()
    {
		yield return new WaitForSeconds (5);

      
    
        //Application.LoadLevel (Application.loadedLevel);
        //SceneManager.LoadScene(0);
    }*/

    void OnTriggerEnter (Collider other)
  {
        // Debug.Log("Collider");
        // If the exiting collider is the player...
        // Debug.Log(other.name);

        if (other.name == "key")
        {
            Debug.Log("Key Sound");
            playerAudio.clip = KeySound;
            playerAudio.Play();
        }
        //if (LayerMask.Equals(PowerUpMask))
        if (other.gameObject.CompareTag("PowerUp") ) //|| (other.gameObject.CompareTag("PowerUp1")  || (other.gameObject.CompareTag("PowerUp2 ") 
           {
            if (currentHealth <= 90)
            {
                currentHealth += 10;
                Destroy(other.gameObject);
                healthSlider.value = currentHealth;
                Debug.Log(currentHealth);
                playerAudio.clip = PowerUpSound;
                playerAudio.Play();
            }
            else if(currentHealth>90 && currentHealth <100){
                currentHealth = 100;
                Destroy(other.gameObject);
                healthSlider.value = currentHealth;
                //Debug.Log(currentHealth);
                playerAudio.clip = PowerUpSound;
                playerAudio.Play();
            }
            //Debug.Log(currentHealth);
            //other.gameObject.SetActive(false);
            Debug.Log(other.name);
           

            //other.gameObject.
            //other.gameObject.en

                // Destroy(other.gameObject, 0.0f);

                // ... the player is no longer in range.
                //  playerInRange = false;

        }


		if (other.gameObject.CompareTag("PowerUp1")) //|| (other.gameObject.CompareTag("PowerUp1")  || (other.gameObject.CompareTag("PowerUp2 ") 
		{
			//Debug.Log(currentHealth);
			other.gameObject.SetActive(false);
			currentHealth += 10;
			healthSlider.value = currentHealth;
			Debug.Log(currentHealth);
			playerAudio.clip = PowerUpSound;
			playerAudio.Play();

			}

		if (other.gameObject.CompareTag("PowerUp2")) //|| (other.gameObject.CompareTag("PowerUp1")  || (other.gameObject.CompareTag("PowerUp2 ") 
		{
			//Debug.Log(currentHealth);
			other.gameObject.SetActive(false);
			currentHealth += 10;
			healthSlider.value = currentHealth;
			Debug.Log(currentHealth);
			playerAudio.clip = PowerUpSound;
			playerAudio.Play();

		}
	}
}