﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

namespace CompleteProject
{
    public class GameOverManager : MonoBehaviour
    {
        public PlayerLife playerHealth;       // Reference to the player's health.


        Animator anim;                          // Reference to the animator component.


        void Awake ()
        {
            // Set up the reference.
            anim = GetComponent <Animator> ();
        }


        void Update ()
        {
            // If the player has run out of health...
            if(playerHealth.currentHealth <= 0)
            {

				//anim.SetTrigger ("GameOver");
				SheduleWaitLevel ();
                // ... tell the animator the game is over.
               
            }
        }

		public void SheduleWaitLevel()
		{
			StartCoroutine (Wait ());
		}

		IEnumerator Wait()
		{
            yield return new WaitForSeconds(3);
            // Reload the level that is currently loaded.

            anim.SetTrigger("GameOver");
            yield return new WaitForSeconds(3);

            SceneManager.LoadScene("Scene1", LoadSceneMode.Single);
           // Application.LoadLevel(Application.loadedLevel);

            //SceneManager.LoadScene(0);
        }
    }
}